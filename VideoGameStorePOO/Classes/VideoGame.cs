﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VideoGameStorePOO.Enums;

namespace VideoGameStorePOO.Classes
{
    public class VideoGame
    {
        string name;
        float price;
        string publisher;
        VGCarac carac;  //mejor diseño, las caracteristicas se van a una clase
        
        

        public string Name { get => name; set => name = value; }
        public float Price { get => price; set => price = value; }
        public string Publisher { get => publisher; set => publisher = value; }
        internal VGCarac Carac { get => carac; set => carac = value; }

        public VideoGame(string name, float price, string publisher, VGCarac caracteristicas)
        {
            
            this.name = name;
            this.price = price;
            this.publisher = publisher;
            carac = new VGCarac();
            carac.Esrb = caracteristicas.Esrb;
            carac.Genero = caracteristicas.Genero;

        }


    }
}
