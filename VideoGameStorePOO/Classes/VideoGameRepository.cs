﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VideoGameStorePOO.Interfaces;
using VideoGameStorePOO;
using VideoGameStorePOO.Enums;

namespace VideoGameStorePOO.Classes
{
    class VideoGameRepository:IRepository<VideoGame>
    {
        //https://blog.falafel.com/implement-step-step-generic-repository-pattern-c/

        List<VideoGame> lista;


        public VideoGameRepository()
        {
            lista = new List<VideoGame>();
            lista.Add(new VideoGame("Doom", 20.0f, "id", new VGCarac(Genero.FPS, ESRB.M)));
            lista.Add(new VideoGame("BOTW", 50.0f, "Nintendo", new VGCarac(Genero.ACTIONRPG, ESRB.E)));
            lista.Add(new VideoGame("Civilization VI", 40.0f, "Firaxis", new VGCarac(Genero.X4, ESRB.E)));
            lista.Add(new VideoGame("Battlefront II", 70.0f, "EA", new VGCarac(Genero.FPS, ESRB.T)));
            lista.Add(new VideoGame("Rocket League", 10.0f, "Psyonix", new VGCarac(Genero.SPORTS, ESRB.E)));
            lista.Add(new VideoGame("FIFA", 10.0f, "Psyonix", new VGCarac(Genero.SPORTS, ESRB.E)));

        }


        public IEnumerable<VideoGame> List
        {
            get
            {
                return lista;
            }

        }

        public void Add(VideoGame entity)
        {
         
        }

        public void Delete(VideoGame entity)
        {
          
        }

        public void Update(VideoGame entity)
        {
          

        }

        public VideoGame FindById(string nombre)
        {

            return new VideoGame("",1,"",new VGCarac(Enums.Genero.ACTIONRPG,Enums.ESRB.E));
        }

        public List<VideoGame> buscar(VGCarac videojuegoToSearchByCaract)
        {
            List<VideoGame> VGEncontrados = new List<VideoGame>();
            foreach (VideoGame videogame in lista)
            {
                VideoGame videogameToAdd = videogame;
                if (videogame.Carac.matches(videojuegoToSearchByCaract))
                    VGEncontrados.Add(videogameToAdd);

            }

            return VGEncontrados;
        }

    }
}
