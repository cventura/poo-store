﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VideoGameStorePOO.Enums;

namespace VideoGameStorePOO.Classes
{
    class Inventario
    {
        List<VideoGame> videogames;


        public Inventario()
        {
            videogames = new List<VideoGame>();
        }

        public void addGame(string name, float price, string publisher, VGCarac caracteristicas )
        {
            VideoGame videogame = new VideoGame(name, price, publisher,caracteristicas);
            videogames.Add(videogame);
        }

        public VideoGame getVideogame(string nameToSearch)
        {
            
            foreach(VideoGame videogame in videogames)
            {
                if(videogame.Name == nameToSearch)
                {
                    return videogame;
                }
            }
            return null;

        }

        public List<VideoGame> buscar(VGCarac videojuegoToSearchByCaract)
        {
            List<VideoGame> VGEncontrados = new List<VideoGame>();
            foreach (VideoGame videogame in videogames)
            {
                VideoGame videogameToAdd = videogame;
                if (videogame.Carac.matches(videojuegoToSearchByCaract))
                    VGEncontrados.Add(videogameToAdd);
        
            }

            return VGEncontrados;
        }


     
    }
}
