﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VideoGameStorePOO.Enums;

namespace VideoGameStorePOO.Classes
{
    public class VGCarac
    {
        Genero genero;
        ESRB esrb;


        public VGCarac()
        {

        }
        
        public VGCarac(Genero genero, ESRB esrb)
        {
            this.genero = genero;
            this.esrb = esrb;
        }

        internal Genero Genero { get => genero; set => genero = value; }
        internal ESRB Esrb { get => esrb; set => esrb = value; }

        public bool matches(VGCarac specs)
        {
            if (specs.Esrb != esrb)
                return false;
            if (specs.Genero != genero)
                return false;

            return true;
        }


    }
}
