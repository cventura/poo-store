﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VideoGameStorePOO.Classes;
using VideoGameStorePOO.Enums;


namespace VideoGameStorePOO
{

    //version 2
    class Program
    {
        static void Main(string[] args)
        {
            Inventario inventario = new Inventario();
            List<VideoGame> encontrados;
            //init(inventario);

            VideoGameRepository repository = new VideoGameRepository();

            VGCarac gameToSearch = new VGCarac(Genero.SPORTS, ESRB.E); //la busqueda ahora esta mas "limpia"

            encontrados = repository.buscar(gameToSearch);

            if(encontrados != null)
            {
                foreach(VideoGame vg in encontrados)
                {
                    Console.WriteLine("Juego {0} ",vg.Name);
                }
            }
            else
            {
                Console.WriteLine("no hay opciones");
            }

        }

        //private static void init(Inventario inventario)
        //{
        //    inventario.addGame("Doom", 20.0f, "id", new VGCarac(Genero.FPS,ESRB.M));
        //    inventario.addGame("BOTW", 50.0f, "Nintendo", new VGCarac(Genero.ACTIONRPG, ESRB.E));
        //    inventario.addGame("Civilization VI", 40.0f, "Firaxis", new VGCarac(Genero.X4, ESRB.E));
        //    inventario.addGame("Battlefront II", 70.0f, "EA",new VGCarac( Genero.FPS, ESRB.T));
        //    inventario.addGame("Rocket League", 10.0f, "Psyonix",new VGCarac(Genero.SPORTS, ESRB.E));
        //    inventario.addGame("FIFA", 10.0f, "Psyonix",new VGCarac(Genero.SPORTS, ESRB.E));

        //}


    }
}
