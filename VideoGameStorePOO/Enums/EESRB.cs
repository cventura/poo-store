﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VideoGameStorePOO.Enums
{

    public enum ESRB { E, T, M}

    static class EESRB
    {
        public static string getString(this ESRB esrb)
        {
            switch (esrb)
            {
                case ESRB.E:
                    return "E";
                case ESRB.T:
                    return "T";
                case ESRB.M:
                    return "M";
                default:
                    return "NA";
            }

        }

    }
}
