﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VideoGameStorePOO.Enums
{

    public enum Genero { FPS, ACTIONRPG, X4 ,SPORTS}

    static class EGenero
    {
        public static string getString(this Genero gen)
        {
            switch (gen)
            {
                case Genero.FPS:
                    return "FPS";
                case Genero.ACTIONRPG:
                    return "Action/RPG";
                case Genero.X4:
                    return "4X";
                case Genero.SPORTS:
                    return "Sports";
                default:
                    return "NA";
            }

        }


    }
}
