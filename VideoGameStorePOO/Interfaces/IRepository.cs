﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VideoGameStorePOO.Classes;

namespace VideoGameStorePOO.Interfaces
{
    public interface IRepository<T>where T:VideoGame
    {
        IEnumerable<T> List { get; }
        void Add(T entity);
        void Delete(T entity);
        void Update(T entity);
        T FindById(string nombre);

    }
}
